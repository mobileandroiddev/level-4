package com.example.annabel.gamebacklog;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

@Entity(tableName = "game")

public class Game implements Parcelable {
    @PrimaryKey(autoGenerate = true)
    private Long ID;

    @ColumnInfo(name = "gameName")
    private String gameName;
    @ColumnInfo(name = "gameNotes")
    private String gameNotes;
    @ColumnInfo(name = "gamePlatform")
    private String gamePlatform;
    @ColumnInfo(name = "gameStatus")
    private String gameStatus;
    @ColumnInfo(name = "gameDate")
    private String gameDate;

    public Game(String gameName,  String gameNotes, String gamePlatform, String gameStatus, String gameDate) {
        this.gameName = gameName;
        this.gamePlatform = gamePlatform;
        this.gameNotes = gameNotes;
        this.gameStatus = gameStatus;
        this.gameDate = gameDate;
    }

    public String getGameName(){
        return gameName;
    }
    public String getGamePlatform(){
        return gamePlatform;
    }
    public String getGameStatus(){
        return gameStatus;
    }
    public String getGameDate(){
        return gameDate;
    }
    public String getGameNotes(){
        return gameNotes;
    }
    public void setGame(String gameName,  String gameNotes, String gamePlatform, String gameStatus, String gameDate){
        this.gameName = gameName;
        this.gamePlatform = gamePlatform;
        this.gameNotes = gameNotes;
        this.gameStatus = gameStatus;
        this.gameDate = gameDate;
    }

    public void setID(long ID) {
        this.ID = ID;
    }
    public Long getID(){
        return ID;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this.ID);
        dest.writeString(this.gameName);
        dest.writeString(this.gamePlatform);
        dest.writeString(this.gameNotes);
        dest.writeString(this.gameStatus);
    }

    protected Game(Parcel in) {
        this.ID = in.readLong();
        this.gameName = in.readString();
        this.gamePlatform = in.readString();
        this.gameNotes = in.readString();
        this.gameStatus = in.readString();
    }

    public static final Creator<Game> CREATOR = new Creator<Game>() {
        @Override
        public Game createFromParcel(Parcel source) {
            return new Game(source);
        }

        @Override
        public Game[] newArray(int size) {
            return new Game[size];
        }
    };

}
