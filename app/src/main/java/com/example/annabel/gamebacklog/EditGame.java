package com.example.annabel.gamebacklog;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.DateFormat;
import java.util.Date;

public class EditGame extends MainActivity {
    private EditText nameInput;
    private EditText platformInput;
    private EditText notesInput;
    private Spinner statusInput;
    private Game editGame;
    private int statusPosition = 3;
    private String status;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);

        nameInput = findViewById(R.id.editName);
        platformInput = findViewById(R.id.editPlatform);
        notesInput = findViewById(R.id.editNotes);
        statusInput = findViewById(R.id.editStatus);
        editGame = getIntent().getParcelableExtra("gameToEdit");
        status = editGame.getGameStatus();

        Log.i("yo",status);

        if (status.equals("Want to play")){
            statusPosition = 0;
        } else if (status.equals("Playing")){
            statusPosition = 1;
        } else if (status.equals("Stalled")){
            statusPosition = 2;
        } else if (status.equals("Dropped")){
            statusPosition = 3;
        }
        statusInput.setSelection(statusPosition);

        nameInput.setText(editGame.getGameName());
        platformInput.setText(editGame.getGamePlatform());
        notesInput.setText(editGame.getGameNotes());


        FloatingActionButton fab = findViewById(R.id.floatingActionButton3);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = nameInput.getText().toString();
                String platform = platformInput.getText().toString();
                String notes = notesInput.getText().toString();
                String status = statusInput.getSelectedItem().toString();
                String date = DateFormat.getDateInstance().format(new Date());

                if (!name.isEmpty() && !platform.isEmpty() && !notes.isEmpty() && !status.isEmpty() && !date.isEmpty()) {
                    editGame.setGame(name, notes, platform, status, date);
                    new MainActivity.GameAsyncTask(UPDATE).execute(editGame);
                    setResult(Activity.RESULT_OK);
                    finish();
                }
            }
        });
    }
}
