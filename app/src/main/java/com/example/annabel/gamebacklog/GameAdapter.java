package com.example.annabel.gamebacklog;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class GameAdapter extends RecyclerView.Adapter<GameAdapter.GameViewHolder> {

    private List<Game> games;
    private GameClickListener gameClickListener;

    public GameAdapter(List<Game> games, GameClickListener gameClickListener) {
        this.games = games;
        this.gameClickListener = gameClickListener;
    }

    public interface GameClickListener {
        void gameOnClick(int i);
    }

    @Override
    public GameViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view, parent, false);
        return new GameAdapter.GameViewHolder(view);
    }

    @Override
    public void onBindViewHolder(GameViewHolder holder, int position) {
        Game game = games.get(position);
        holder.gameNameCard.setText(game.getGameName());
        holder.gamePlatformCard.setText(game.getGamePlatform());
        holder.gameDateCard.setText(game.getGameDate());
        holder.gameStatusCard.setText(game.getGameStatus());
    }

    @Override
    public int getItemCount() {
        return games.size();
    }

    public void setGames(List games) {
        this.games = games;
        this.notifyDataSetChanged();
    }

    public class GameViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView gameNameCard;
        TextView gamePlatformCard;
        TextView gameDateCard;
        TextView gameStatusCard;

        public GameViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            gameStatusCard = itemView.findViewById(R.id.gameStatus);
            gameNameCard = itemView.findViewById(R.id.gameName);
            gamePlatformCard = itemView.findViewById(R.id.gamePlatform);
            gameDateCard = itemView.findViewById(R.id.gameDate);
        }

        @Override
        public void onClick(View view) {
            int clickedPosition = getAdapterPosition();
            gameClickListener.gameOnClick(clickedPosition);
        }
    }

}