package com.example.annabel.gamebacklog;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;

import java.text.DateFormat;
import java.util.Date;

public class AddGame extends MainActivity {
    private EditText nameInput;
    private EditText platformInput;
    private EditText notesInput;
    private Spinner statusInput;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        nameInput = findViewById(R.id.addName);
        platformInput = findViewById(R.id.addPlatform);
        notesInput = findViewById(R.id.addNotes);
        statusInput = findViewById(R.id.addStatus);

        FloatingActionButton fab = findViewById(R.id.floatingActionButton2);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = nameInput.getText().toString();
                String platform = platformInput.getText().toString();
                String notes = notesInput.getText().toString();
                String status = statusInput.getSelectedItem().toString();
                String date = DateFormat.getDateInstance().format(new Date());

                if (!name.isEmpty() && !platform.isEmpty() && !notes.isEmpty() && !status.isEmpty() && !date.isEmpty()) {
                    Game game = new Game(name, notes, platform, status, date);
                    new GameAsyncTask(INSERT).execute(game);
                    setResult(RESULT_OK);
                    finish();
                }
            }
        });
    }
}

