package com.example.annabel.gamebacklog;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements GameAdapter.GameClickListener {

    private List<Game> games;
    private GameAdapter adapter;
    private RecyclerView recyclerView;
    static AppDatabase database;
    public final static int GET_ALL = 0;
    public final static int DELETE = 1;
    public final static int UPDATE = 2;
    public final static int INSERT = 3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        database = AppDatabase.getInstance(this);
        new GameAsyncTask(GET_ALL).execute();
        games = new ArrayList<>();
        games.add(new Game("The Witcher 3", " ", "PC", "Playing", "31/10/2017"));
        games.add(new Game("Overwatch", " ", "PC", "Dropped", "31/10/2017"));

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        adapter = new GameAdapter(games, this);
        recyclerView.setAdapter(adapter);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddGame.class);
                startActivityForResult(intent, 1111);
            }
        });

        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) { return false; }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int position = (viewHolder.getAdapterPosition());
                new GameAsyncTask(DELETE).execute(games.get(position));
            }
        };

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1111) {
            if (resultCode == RESULT_OK) {
                new GameAsyncTask(GET_ALL).execute();
                updateUI();
            }
        }
        else if (requestCode == 2222) {
            if (resultCode == RESULT_OK) {
                new GameAsyncTask(GET_ALL).execute();
                updateUI();
            }
        }
    }


    @Override
    public void gameOnClick(int i) {
        Intent intent = new Intent(MainActivity.this, EditGame.class);
        intent.putExtra("gameToEdit", games.get(i));
        intent.putExtra("position", i);
        startActivityForResult(intent, 2222);
    }

    public class GameAsyncTask extends AsyncTask<Game, Void, List> {
        private int task;

        public GameAsyncTask(int task) {
            this.task = task;
        }

        @Override
        protected List doInBackground(Game... games) {
            switch (task) {
                case DELETE:
                    database.gameDao().deleteGames(games[0]);
                    break;

                case UPDATE:
                    database.gameDao().updateGames(games[0]);
                    break;

                case INSERT:
                    database.gameDao().addGames(games[0]);
                    break;
            }
            return database.gameDao().getGames();
        }

        @Override
        protected void onPostExecute(List list) {
            super.onPostExecute(list);
            onDatabaseUpdate(list);
        }
    }

    public void onDatabaseUpdate(List list) {
        games = list;
        updateUI();
    }

    private void updateUI() {
        adapter.setGames(games);
    }
}
